/*!
 * @license
 * Copyright (c) 2015 Devoteam Fringes.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
var
    version = '0.0.1',

    /**
     * Returns a formatted string using the specified locale, format string, and arguments.
     * @param src - A format string.
     * @param {string} args - Arguments referenced by the format specifiers in the format string.
     * @returns {string} A formatted string
     * @private
     */
    _formatString = function (src, args) {
        // The string containing the format items (e.g. '{0}')
        // will and always has to be the first argument.
        var result = arguments[0];

        // start with the second argument (i = 1)
        for (var i = 1; i < arguments.length; i++) {
            // 'gm' = RegEx options for Global search (more than one instance)
            // and for Multiline search
            var regEx = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
            result = result.replace(regEx, arguments[i]);
        }

        return result;
    };

/**
 * Creates a new instance of jGSA
 * @constructor
 * @param {string} server - Google Search Appliance host name or IP address, which were assigned when the search appliance was set up
 * @param {number=} [port=80 or 443] - Search interface port (default HTTP serving port: 80 for HTTP and 443 for HTTP over SSL/TLS)
 * @param {string=} [client=none] - The results will be rendered into HTML format using the XSL stylesheet associated with the front end specified in the client
 * @param {(string|string[])=} [collections=none] - The results will contain the first page of search results for the query string restricted to URLs in the collection(s)
 *
 * @example
 * // A simple jGSA manager with only the server hostname or IP address
 * var myGSAManager = new jGSA('localhost');
 *
 * // Also, you can set an specific port, front end and collection(s)
 * var myGSAManager = new new jGSA('localhost','443','json_frontend', 'collection1');
 *
 * // If you have multiple collections, you can pass them as an array
 * var myGSAManager = new jGSA('localhost','443','json_frontend', ['collection1', 'collection2']);
 */
var jGSA = function (server, port, client, collections) {
    this._baseurl = 'http://{0}/search';
    this._baseapi = 'site={0}&client={1}&proxystylesheet={1}';

    // HANDLE: jGSA(""), jGSA(null), jGSA(undefined), jGSA(false)
    if (!server) {
        console.warn('Google Search Appliance host name not provided');

        return;
    }

    return this._init(server, port, client, collections);
};

jGSA.fn = jGSA.prototype = {
    jgsa: version,

    constructor: jGSA,

    _formatUrl: function () {
        return _formatString(this._baseurl, this._port ? this._server + ":" + this._port : this._server);
    },

    _formatApi: function () {
        return _formatString(this._baseapi, this._collections, this._client);
    },

    _encodeCollections: function (collections) {
        if (!collections) {
            return '';
        } else if (typeof collections === 'string') {
            return collections;
        } else if (collections[0]) {
            var collectionsEncoded = '';
            collections.forEach(function (element) {
                collectionsEncoded = collectionsEncoded ? collectionsEncoded + "|" + element : element;
            });

            return collectionsEncoded;
        }
    },

    _init: function (server, port, client, collections) {
        return this.server(server)
            .port(port)
            .client(client)
            .collections(collections);
    },

    /**
     * Version of jGSA Library
     * @returns {string} Version of jGSA Library
     */
    version: function () {
        return this.jgsa;
    },

    /**
     * Changes server host name or IP address
     * @param {string} server - Google Search Appliance host name or IP address, which were assigned when the search appliance was set up
     * @returns {jGSA} Self object
     */
    server: function (server) {
        this._server = server;

        return this;
    },

    /**
     * Changes port
     * @param {number} port - Search interface port (default HTTP serving port: 80 for HTTP and 443 for HTTP over SSL/TLS)
     * @returns {jGSA} Self object
     */
    port: function (port) {
        this._port = port;

        return this;
    },

    /**
     * Changes client
     * @param {string} client - The results will be rendered into HTML format using the XSL stylesheet associated with the front end specified in the client
     * @returns {jGSA} Self object
     */
    client: function (client) {
        this._client = client ? client : '';

        return this;
    },

    /**
     * Change collection(s)
     * @param {string|string[]} collections - The results will contain the first page of search results for the query string restricted to URLs in the collection(s)
     * @returns {jGSA} Self object
     */
    collections: function (collections) {
        this._collections = this._encodeCollections(collections);

        return this;
    },

    /**
     * @callback jGSA.GSASearchCallBack
     * @param {jGSA.GSAResponse} response - A plain object or string that is received from the server
     * @param {number} status - HTTP status code
     * @param {string} textStatus - HTTP status message
     */

    /**
     * @typedef {object} jGSA.GSAResponse
     * @property {string} TM - Total server time to return search results, measured in seconds.
     * @property {string} Q - The search query terms submitted to the Google search appliance to generate these results.
     * @property {object[]} PARAM - The search request parameters that were submitted to the Google Search Appliance to generate these results.
     * @property {string} PARAM.name - Name of the input parameter.
     * @property {string} PARAM.value - HTML-formatted version of the input parameter value.
     * @property {string} PARAM.original_value - Original URL-encoded version of the input parameter value.
     * @property {object} PARM - Encapsulates all dynamic navigation results.
     * @property {object[]} PARM.PMT - Encapsulates all dynamic navigation results.
     * @property {object} RES - Encapsulates the set of all search results.
     * @property {object} RES.NB - Not included in API. Encapsulates the navigation information for the result set. The NB tag is present only if either the previous or additional results are available.
     * @property {object} RES.NB.PU - Not included in API. Contains relative URL to the previous results page.
     * @property {object} RES.NB.NU - Not included in API. Contains a relative URL pointing to the next results page.
     * @property {object} RES.SN - The index (1-based) of the first search result returned in this result set.
     * @property {object} RES.EN - Indicates the index (1-based) of the last search result returned in this result set.
     * @property {object} RES.M - The estimated total number of results for the search.
     * @property {object[]} RES.R - Encapsulates the details of an individual search result.
     * @property {number} RES.R.N - The index number (1-based) of this search result.
     * @property {string} RES.R.U - The URL of the search result.
     * @property {string} RES.R.T - The title for the search result in html format.
     * @property {string} RES.R.S - The snippet for the search result in html format.
     * @property {object} RES.R.MT - Meta tag name and value pairs obtained from the search result.
     * @property {object} RES.R.LANG - Indicates the language of the search result. The LANG element contains a two-letter language code.
     */

    /**
     * TODO
     * The Google search request is a standard HTTP POST command, which returns results in either XML ,HTML or JSON format, as specified in the search request
     *
     * The search request is a URL that combines the following:
     *  - Your Google Search Appliance host name or IP address, which were assigned when the search appliance was set up
     *  - Search interface port (default HTTP serving port: 80 for HTTP and 443 for HTTP over SSL/TLS)
     *  - A path describing the search query. The path starts with “/search?”, and is followed by one or more name-value pairs (input parameters) separated by the ampersand (&) character.
     *
     * POST support is only available for
     *  - Request for search service (/search)
     *  - Public search
     *  - Secure search (but only for cookie and basic authentication, and when Trusted Applications feature is used)
     * @param {string} query - Google Search Appliance query string
     * @param {jGSA.GSASearchCallBack} callback - A callback function that is executed if the request succeeds.
     * @return {string} body - POST body of the request.
     *
     * @example
     * // create query
     * myQueryBuilder.clear();
     * var myQueryString = myQueryBuilder
     *     .start(0, 10) // start from 0 and return 10 results
     *     .terms(userInput) // terms
     *     .inmeta('surname', value) // inmeta value
     *     .inmetaRange('age', minAge, maxAge) // range
     *     .inmetaRange('height', minHeight, maxHeight) // range
     *     .build(); // generate query string
     * // perform search
     * myGSAManager.search(myQueryString, function(response, status, textStatus) {
     *     var results = [];
     *     try {
     *         if (response && typeof response === "object") {
     *             var t = response.TM;
     *             var q = response.Q;
     *             //...handle response...
     *         } else {
     *             console.error('Error parsing GSA response', response);
     *         }
     *     catch (err) {
     *         console.error('Error parsing GSA response', response);
     *     }
     * }
     *
     * @see jGSA.jGSAQueryBuilder
     * @see jGSA.GSAResponse
     */
    search: function (query, callback) {
        var url = this._formatUrl();
        var api = this._formatApi();

        // TODO - DELETE
        console.info("URL: %s (%s)", url, "GET");
        console.info("BODY: %s", api + query);

        $.ajax({
            type: "GET",
            url: '/search',
            data: {
                url: url + "?" + api + query,
            },
            success: cb,
            error: err
        });

        return api + query;

        function cb(data, textStatus, jqXHR) {
            try {
                callback(data.GSP, 200, textStatus);
            } catch (err) {
                console.error(err);
            }
        }

        function err(jqXHR, textStatus, errorThrown) {
            callback(undefined, 500, errorThrown);
        }
    },

    /**
     * The query suggestion service provides suggestions that complete a user’s search query. As a user enters a query in a search box, a drop-down menu appears with suggestions to complete the query.
     * The search appliance uses the most popular search queries of its users to determine the top suggestions that list for a query.
     * Only queries that returned results are used to build the database of query suggestions. Queries with special terms, such as inmeta:, info:, link:, daterange:, etc are excluded when building the database of query suggestions.
     * In addition, if activated, the search appliance adds user-added results to the list of suggestions.
     * @param input - id of the input field
     * @param max - Maximum number of results to include in the suggest results
     *
     * @example
     * TODO
     */
    suggest: function (input, max) {
        if (!input) return;

        var server = this._server;
        var collection = this._collections;
        var client = this._client;
        var field = $('#' + input);

        field.autocomplete({
            source: function (request, response) {
                var query = field.val();
                var url = "http://" + server + "/suggest";
                var api = "?q=" + query + "&max=" + max + "&site=" + collection + "&client=" + client + "&format=rich";

                // TODO - DELETE
                console.info("URL: %s (%s)", url, "GET");
                console.info("PARAMETERS: %s", api);

                $.ajax({
                    dataType: "jsonp",
                    jsonp: "callback",
                    type: "GET",
                    url: url + api,
                    success: function (data) {
                        var results = [];

                        data.results.forEach(function (element) {
                            if (element.type === "suggest") {
                                results.push(element.name);
                            }
                        });

                        response(results);
                    },
                    error: function () {
                        console.error('Could not connect to GSA Server: %s', url);
                    }
                })
            },
            timeout: 3000,
            minLength: 2
        });
    }
};

/**
 * Creates a new instance of jMaps
 * @constructor
 *
 * @example
 * // Example of a simple jMaps
 * var myMaps = new jGSA.jMaps();
 *
 * // Get current location and print the result
 * myMaps.geolocation(function (latLng) {
 *     console.log('Lat: ' + latLng.lat() + ' Lng: ' + latLng.lng());
 *
 *     // Calculate a rectangle area of 5km around the current location
 *     var area = myMaps.latLngBounds(latLng.lat(), latLng.lng(), 5000, 5000);
 *     var bounds = area.getBounds();
 *
 *     // Print rectangle corners
 *     console.log('UpLeft=' + bounds.getSouthWest().lat() + " ~ " + bounds.getNorthEast().lng());
 *     console.log('UpRight=' + bounds.getNorthEast().lat() + " ~ " + bounds.getNorthEast().lng());
 *     console.log('DownLeft=' + bounds.getSouthWest().lat() + " ~ " + bounds.getSouthWest().lng());
 *     console.log('DownRight=' + bounds.getNorthEast().lat() + " ~ " + bounds.getSouthWest().lng());
 * });
 */
jGSA.jMaps = function () {
    //

    return this;
};

jGSA.jMaps.fn = jGSA.jMaps.prototype = {
    constructor: jGSA.jMaps,

    /**
     * @callback jGSA.jMaps.geolocationCallback
     * @param {google.maps.LatLng} latLng - Google Maps LatLng if geolocalization has been successful, and null if it hasn't
     *
     * @see {@link https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=es#LatLng|LatLng}
     */

    /**
     * Determine geolocation object that gives Web content access to the location of the device.
     * This allows a Web site or app to offer customized results based on the user's location.
     * @param {jGSA.jMaps.geolocationCallback} callback
     *
     * @example
     * // Get current location and print the result
     * myMaps.geolocation(function (latLng) {
     *     console.log('Lat: ' + latLng.lat() + ' Lng: ' + latLng.lng());
     * });
     *
     * // You can associate geolocation to a button in your web
     * $('#my_button').click(function () {
     *     myMaps.geolocation(function (latLng) {
     *         console.log('Lat: ' + latLng.lat() + ' Lng: ' + latLng.lng());
     *         // do stuff
     *     });
     * });
     */
    geolocation: function (callback) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                if (callback && typeof callback === 'function')
                    callback(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            }, function () {
                console.error('geolocation failed');
                if (callback && typeof callback === 'function') {
                    callback(null);
                }
            })
        } else {
            console.warn('your browser doesn\'t support geolocation');
            if (callback && typeof callback === 'function') {
                callback(null);
            }
        }
    },

    /**
     * @callback jGSA.jMaps.autocompleteCallback
     * @param {google.maps.PlaceResult} place - Google Maps PlaceResult
     *
     * @see {@link https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=es#PlaceResult|PlaceResult}
     */

    /**
     * A service to provide Place predictions based on a user's text input.
     * It attaches to an input element of type text, and listens for text entry in that field.
     * The list of predictions is presented as a drop-down list, and is updated as text is entered.
     * @param {string} input - id of the input field
     * @param {jGSA.jMaps.autocompleteCallback} callback - function called when user selects a location
     *
     * @return {google.maps.places.Autocomplete} Google Maps Autocomplete
     *
     * @example
     * // Set our input id
     * var myInputId = 'input_address';
     * // create an autocomplete object. If you have specified a callback,
     * // automatically the event 'place_changed' will call upit callback with the place selected by the user
     * var autocomplete = myMaps.autocomplete(myInputId, function (place) {
     *       if (!place.geometry) {
     *           // If place has no geometry, we can't determine the location
     *           console.warn('place does not contains geometry')
     *       } else {
     *           // If place has geometry we can draw it on map with a marker for example
     *           var myMarker = new google.maps.Marker({position: place.geometry.location})
     *           // draw marker on map
     *           // ...
     *       }
     *   });
     *
     * @see {@link https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=es#Autocomplete|Autocomplete}
     */
    autocomplete: function (input, callback) {
        if (!input) return null;

        return new google.maps.places.Autocomplete($('#' + input).get(0), {})
            .addListener('place_changed', function () {
                var place = this.getPlace();
                if (!place.geometry) {
                    console.warn('Autocomplete\'s returned place contains no geometry');
                }

                if (callback && typeof callback === 'function') {
                    callback(place);
                }
            });
    },

    /**
     * Computes the geographical coordinates of a rectangle area which has the center in the given latitude and longitude.
     * The width of the rectangle is determined by parameter dx and the height is determined by parameter dy.
     * @param {number} lat - latitude in degrees
     * @param {number} lon - longitude in degrees
     * @param {number=} [dx=1000] - distance in meters on X axis
     * @param {number=} [dy=1000] - distance in meters on Y axis
     * @returns {google.maps.Rectangle} Google Maps Rectangle
     *
     * @example
     * // Get an area of 1000 meters around given longitude and latitude
     * var area = myMaps.latLngBounds(40.3915015, -3.6890343999999686);
     * // Print the result
     * var bounds = area.getBounds();
     * console.log('Center=' + bounds.getCenter().lat() + ' ~ ' + bounds.getCenter().lng());
     * console.log('North East=' + bounds.getNorthEast().lat() + ' ~ ' + bounds.getNorthEast().lng());
     * console.log('South West=' + bounds.getSouthWest().lat() + ' ~ ' + bounds.getSouthWest().lng());
     *
     * @see {@link https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=es#Rectangle|Rectangle}
     */
    latLngBounds: function (lat, lon, dx, dy) {
        var R = 6371000;

        if (!lat || !lon) return null;

        dx = dx ? dx : 1000;
        dy = dy ? dy : 1000;

        var maxLat = lat + (dy / R) * (180 / Math.PI);
        var minLat = lat - (dy / R) * (180 / Math.PI);
        var maxLng = lon + (dx / R) * (180 / Math.PI) / Math.cos(lat * Math.PI / 180);
        var minLng = lon - (dx / R) * (180 / Math.PI) / Math.cos(lat * Math.PI / 180);

        return new google.maps.Rectangle({
            bounds: {
                north: maxLat,
                south: minLat,
                east: maxLng,
                west: minLng
            }
        });
    }
};

/**
 * Creates a new instance of jGSAQueryBuilder
 * @constructor
 *
 * @example
 * // Example of a simple jGSAQueryBuilder
 * var myGSAQueryBuilder = new jGSA.jGSAQueryBuilder();
 *
 * // Set search restrictions
 * var myQueryString = myGSAQueryBuilder.terms('peter smith')
 *     .inmeta('name', 'peter')
 *     .inmeta('surname', 'smith')
 *     .inmetaRange('age', 0, 36)
 *     .inmetaRange('height', 170, 180)
 *     .build();
 *
 * // Show the result
 * console.log(myQuery);
 */
jGSA.jGSAQueryBuilder = function () {
    this._q = '&q={0}';
    this._qinmeta = '+inmeta:{0}={1}';
    this._qinmetarange = '+inmeta:{0}:{1}';
    this._qstart = '&start={0}&num={1}';
    this._qgetfields = '&getfields={0}';

    return this;
};

jGSA.jGSAQueryBuilder.fn = jGSA.jGSAQueryBuilder.prototype = {
    constructor: jGSA.jGSAQueryBuilder,

    _query: '',
    _inmeta: '',
    _sort: '',
    _start: '',
    _getfields: '',

    /**
     * Restricts results to required term(s)
     * @param {string} terms
     * @returns {jGSA.jGSAQueryBuilder} Self object
     *
     * @example
     * // Create terms restriction
     * myGSAQueryBuilder.terms('peter smith');
     *
     * // Append more terms calling again terms function
     * myGSAQueryBuilder.terms('paul mendez')
     *     .terms('joe');
     *
     * // Print the result
     * console.log(myGSAQueryBuilder.build());
     */
    terms: function (terms) {
        var encoded = '';

        if (!terms || typeof  terms !== 'string') {
            console.warn('incorrect terms information', terms);

            return this;
        }

        var termsArray = terms.split(' ');
        if (termsArray[0]) {
            termsArray.forEach(function (element, index, array) {
                encoded = encoded + encodeURIComponent(element);
                if (termsArray[index + 1]) {
                    encoded = encoded + '+';
                }
            });
        }

        this._query = this._query ? this._query + "+" + encoded : _formatString(this._q, encoded);

        return this;
    },

    /**
     * Restricts results to required meta tag value(s)
     * @param {string} meta - Key(s) of meta tag(s)
     * @param {string} value - Value(s) of meta information
     * @returns {jGSA.jGSAQueryBuilder} Self object
     *
     * @example
     * // Create inmeta restriction
     * myGSAQueryBuilder.inmeta('name', 'peter');
     *
     * // Append inmeta restrictions calling again inmeta function
     * myGSAQueryBuilder.inmeta('surname', 'mendez');
     *
     * // Print the result
     * console.log(myGSAQueryBuilder.build());
     */
    inmeta: function (meta, value) {
        if (!meta || typeof meta !== 'string') {
            console.warn('incorrect meta/value information', meta, value);

            return this;
        }

        if (value == '') {
            return this;
        }

        this._inmeta = this._inmeta + _formatString(this._qinmeta, meta, encodeURIComponent(value));

        return this;
    },

    /**
     * Restricts results to required meta tag value(s) within a range number
     * @param {string} meta - Key(s) of meta tag(s)
     * @param {number} min - Min value(s) of meta information
     * @param {number} max - Max value(s) of meta information
     * @returns {jGSA.jGSAQueryBuilder} Self object
     *
     * @example
     * // Create inmeta restriction to fetch results that have meta data 'age' between 18 and 36
     * myGSAQueryBuilder.inmetaRange('age', '18', '36');
     *
     * // Append more restrictions calling again inmetaRange function
     * myGSAQueryBuilder.inmetaRange('height', '170', '180');
     *
     * // Print the result
     * console.log(myGSAQueryBuilder.build());
     */
    inmetaRange: function (meta, min, max) {
        if (!meta || typeof meta !== 'string' || typeof min !== 'number' || typeof max != 'number') {
            console.warn('incorrect meta/values information', meta, min, max);

            return this;
        }

        this._inmeta = this._inmeta + _formatString(this._qinmetarange, meta, Number((min).toFixed(6)) + '..' + Number((max).toFixed(6)));

        return this;
    },

    /**
     * Use the getFields in a search request to specify meta tag values to return with the search results.
     * The search engine returns only meta tag information for results that actually contain the meta tags.
     * The search for meta tags is case-insensitive.
     * @param {string|string[]} metaTags - Meta tag names to return with the search result. IF set to '*' all fields will be returned
     * @returns {jGSA.jGSAQueryBuilder} Self object
     *
     * @example
     * TODO
     */
    getFields: function (metaTags) {
        if (!metaTags || !metaTags[0]) {
            console.warn('incorrect meta tag names', metaTags);

            return this;
        }

        var meta = '';

        if (metaTags instanceof Array) {
            metaTags.forEach(function (element, index, array) {
                meta = meta + element;
                if (metaTags[index + 1]) meta = meta + ".";
            });
        } else {
            meta = metaTags;
        }

        if (meta != '') {
            this._getfields = _formatString(this._qgetfields, meta);
        }

        return this;
    },

    /**
     * Specifies the index number of the first entry in the result set that is to be returned.
     * Use from parameter and the num parameter to implement page navigation for search results.
     * The index number of the results is 0-based.
     * The maximum number of results available for a query is 1,000, i.e., the value of the start parameter added to the value of the num parameter cannot exceed 1,000.
     * @param {number} from - Index number of the first entry
     * @param {number=} [num=10] - Maximum number of results to include in the search results
     * @returns {jGSA.jGSAQueryBuilder} Self object
     *
     * @example
     * var myQueryString = myGSAQueryBuilder
     *     .terms('peter smith')// create terms restriction
     *     .start(26, 25) // retrieve the next 25 results from result number 25 (i.e. the second page of a 25 result pagination)
     *     .build();
     *
     * // print result
     * console.log(myQueryString);
     */
    start: function (from, num) {
        if (typeof from !== 'number') {
            console.warn('incorrect start value information', from);

            return this;
        }

        if (!num) num = 10;

        this._start = _formatString(this._qstart, from, num);

        return this;
    },

    /**
     * Generates a Google Search Appliance query that includes the provided search term(s), inmeta filters and start options
     * @returns {string} Google Search Appliance query string
     *
     * @example
     * // Get the query from the query builder
     * var myQueryString = myGSAQueryBuilder.build();
     */
    build: function () {
        var string = '';

        if (this._query != '') string = string + this._query;
        if (this._inmeta != '') string = string + this._inmeta;
        if (this._start != '') string = string + this._start;
        if (this._getfields != '') string = string + this._getfields;

        return string;
    },

    /**
     * Clears all terms and inmeta information of the query.
     * This method is intended to re-utilize the jGSAQueryBuilder for multiple searches
     * instead of create a new one
     *
     * @example
     * // New terms
     * myGSAQueryBuilder.terms('peter smith');
     * // ** execute search in GSA **
     *
     * // Clear the terms so you do not have to create another query builder
     * myGSAQueryBuilder.clear();
     *
     * // New terms
     * myGSAQueryBuilder.terms('paul mendez');
     * // ** execute search in GSA **
     */
    clear: function () {
        this._query = '';
        this._inmeta = '';
    }
};