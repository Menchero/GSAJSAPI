/*!
 * Created by Pedro Menchero on 10/03/2016.
 */

// default map location & options
var _defaultPosition = new google.maps.LatLng(40.448571, -3.677651);
var _defaultOptions = {zoom: 13, mapTypeId: google.maps.MapTypeId.ROADMAP};
var _defaultGSAServer = {server: '10.10.121.99', port: 80, collection: 'default_collection', client: 'aegon_json'};

// google.maps.Map
var _map;

// markers displayed on map
var _markersOnMap = [];

// area location & options
var _areaCenter;
var _areaOnMap;
var _areaDistance = 2000;
var _areaStrokeColor = '#0077C8';
var _areaFillColor = '#0AB9EB';
var _areaVisible = true;

// jgsa library objects
var _jgsa = new jGSA(_defaultGSAServer.server, _defaultGSAServer.port, _defaultGSAServer.client, _defaultGSAServer.collection);
var _jqueryBuilder = new jGSA.jGSAQueryBuilder();
var _jmaps = new jGSA.jMaps();

// initialize
function initialize() {
    // initialize map
    _map = new google.maps.Map($('#map').get(0), _defaultOptions);

    // initialize the location
    _location();

    // initialize specialist list
    _specialist();

    // initialice search and clear all buttons
    _search();
    _clearAll();

    // draws default
    _defaultMap();
}

// initialize autocomplete field and geolocation button
function _location() {
    // autocomplete input address
    var inputId = 'input_address';
    var autocomplete = _jmaps.autocomplete(inputId, function (place) {
        if (!place.geometry) {
            // If place has no geometry, we can't determine the location
            console.warn('place does not contains geometry')
        } else {
            var markers = [
                new google.maps.Marker({position: place.geometry.location})
            ];
            _drawOnMap(place.geometry.location, markers);
        }
    });

    // range of km
    var slider = $("#slider_km_range");

    slider.slider({
        range: "max",
        min: 1,
        max: 50,
        value: 2,
        slide: function (event, ui) {
            $("#input_km_range_value").val('Area: ' + ui.value + 'km');
            _areaDistance = ui.value * 1000;
            _drawOnMap();
        }
    });
    $("#input_km_range_value").val('Area: ' + slider.slider("value") + 'km');

    // geolocation btn
    $('#btn_geolocation').click(function () {
        _jmaps.geolocation(function (latLng) {
            if (latLng) {
                _drawOnMap(latLng, [new google.maps.Marker({position: latLng})]);
            } else {
                _defaultMap();
            }
        });
    });
}

// initialize specialist erase and dropdown list
function _specialist() {
    // erase button
    $('#btn_erase_specialist').click(function () {
        $('#input_specialist').val('');
    });

    // dropdown list
    // todo - get specialists of aegon
    var specialists = ['Fisioterapia', 'Pediatria', 'Otorrinolaringologia', 'ATS'];
    var specialistList = $('#ul_specialist_list');

    specialists.forEach(function (element) {
        specialistList.append(generateLi(element));
    });

    specialistList.click(function (event) {
        $('#input_specialist').val(event.target.text);
    });

    function generateLi(value) {
        return $('<li>').append($('<a>').attr('href', '#').append(value));
    }
}

// initialize search button and free search input
function _search() {
    // search button
    var btnSearch = $('#btn_search');

    btnSearch.click(search);

    // free text enter
    var freeTextInput = $('#input_free_text_search');

    freeTextInput.keypress(function (e) {
        if (e.which == 13) {
            search();
        }
    });

    _jgsa.suggest('input_free_text_search', 15);

    function search() {
        var input = $('#input_free_text_search');
        var specialist = $('#input_specialist');

        var northEast = _areaOnMap.getBounds().getNorthEast();
        var southWest = _areaOnMap.getBounds().getSouthWest();

        if (input.val() != '') {
            // create query
            _jqueryBuilder.clear();
            var query = _jqueryBuilder
                .start(0, 25)
                .terms(input.val())
                .inmeta('especialidad', specialist.val())
                .inmetaRange('latitud', southWest.lat(), northEast.lat())
                .inmetaRange('longitud', southWest.lng(), northEast.lng())
                .getFields('*')
                .build();

            // perform search
            var completeQuery = _jgsa.search(query, function (response, status, textStatus) {
                console.info(response);

                var results = [];
                var dynamic = [];
                try {
                    if (response && typeof response === "object") {
                        var t = response.TM;
                        var q = response.Q;
                        if (response.RES.PARM !== undefined && response.RES.PARM.PMT[0]) {
                            response.RES.PARM.PMT.forEach(function (element, index, array) {
                                dynamic[index] = {
                                    title: element.DN,
                                    values: element.PV
                                }
                            })
                        }
                        if (response.RES !== undefined && response.RES.R[0]) {
                            response.RES.R.forEach(function (element, index, array) {
                                results[index] = {
                                    index: element.N,
                                    title: element.MT.nombre,
                                    city: element.MT.poblacion + '(' + element.MT.provincia + ')',
                                    location: element.MT.cdpostal + ', ' + element.MT.domicilio,
                                    phone: element.MT.telefono,
                                    link: element.U
                                };
                            });
                        }
                    } else {
                        console.error('Error parsing GSA response', response);
                    }
                } catch (err) {
                    console.error('Error parsing GSA response', response);
                } finally {
                    updateDynamicSearch(dynamic);
                    updateResultList(results);
                    updateQueryLabel(completeQuery);
                }
            });
        } else {
            updateDynamicSearch();
            updateResultList();
            updateQueryLabel();
        }

        // update dynamic search
        function updateDynamicSearch(list) {
            var dynamicTags = $('#list_dynamic_search');

            // clear previous results
            dynamicTags.empty();

            // create new results
            if (list !== undefined && list[0]) {
                list.forEach(function (element, index, array) {
                    dynamicTags.append($('<h5>').html(element.title));

                    if (element.values !== undefined && element.values[0]) {
                        try {
                            element.values.forEach(function (element2, index2, array2) {
                                var item = $('<li>')
                                    .append($('<a>').attr('href', 'javascript:console.log("inmeta:' + element.title + '=' + element2.V + '")')
                                        .html(element2.V + "(" + element2.C + ")"));

                                if (index2 < 3) dynamicTags.append(item);
                                else {
                                    dynamicTags.append($('<li>')
                                        .append($('<a>').attr('href', '#').html("more (" + array2.length + ")")));
                                    throw BreakException;
                                }
                            });
                        } catch (e) {
                            // ignore
                        }
                    }
                });
            }
        }

        // update result list
        function updateResultList(results) {
            var resultList = $('#list_results');

            // clear previous results
            resultList.empty();

            // create new results
            if (results !== undefined && results[0]) {
                results.forEach(function (element, index, array) {
                    var item = $('<a>').attr('href', '#').attr('class', 'list-group-item')
                        .append($('<h4>').attr('class', 'list-group-item-heading').html(element.title))
                        .append($('<p>').attr('class', 'list-group-item-text').html(element.city))
                        .append($('<p>').attr('class', 'list-group-item-text').html(element.location))
                        .append($('<p>').attr('class', 'list-group-item-text').html(element.phone));
                    if (index < 5) resultList.append(item);
                });
            }
        }

        // update query label
        function updateQueryLabel(query) {
            var queryLabel = $('#label_debug_tag');

            if (query !== undefined && query != '') {
                queryLabel.text(query);
            } else {
                queryLabel.text("{{Busqueda}}");
            }
        }
    }
}

// initialize clear all button
function _clearAll() {
    $('#btn_clear_all').click(function () {
        var inputFreeText = $('#input_free_text_search');
        var location = $('#input_address');
        var specialist = $('#input_specialist');
        var debugTag = $('#label_debug_tag');

        inputFreeText.val('');
        location.val('');
        specialist.val('');
        debugTag.text('');
    });
}

// draws the default map
function _defaultMap() {
    _drawOnMap(_defaultPosition, [new google.maps.Marker({position: _defaultPosition})]);
}

// center the map in the specified position and draw the markers
// call this function without arguments to refresh the map
function _drawOnMap(position, markers) {
    if (position !== undefined && position.lat && position.lng) {
        _areaCenter = position;
    } else if (!_areaCenter) {
        console.warn('not specific position, using default position');
        _areaCenter = _defaultPosition;
    }

    if (_areaVisible) {
        var rectangle = _jmaps.latLngBounds(_areaCenter.lat(), _areaCenter.lng(), _areaDistance, _areaDistance);

        if (_areaOnMap !== undefined) {
            _areaOnMap.setBounds(rectangle.getBounds());
        } else {
            _areaOnMap = new google.maps.Rectangle({
                strokeColor: _areaStrokeColor,
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: _areaFillColor,
                fillOpacity: 0.35,
                map: _map,
                bounds: rectangle.getBounds()
            });
        }
    }

    // draw new markers
    if (markers !== undefined && markers[0]) {
        // remove old markers
        if (_markersOnMap[0]) {
            for (var index in _markersOnMap) {
                _markersOnMap[index].setMap(null);
            }
        }
        // create the new ones
        for (var index in markers) {
            // add new ones
            _markersOnMap[index] = new google.maps.Marker({
                position: markers[index].position,
                animation: google.maps.Animation.DROP,
                map: _map
            });
        }
    }

    _map.setCenter(_areaCenter);
}