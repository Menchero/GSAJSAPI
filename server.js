var express = require("express"),
    app = express(),
    request = require("request"),
    path = require('path');

app.use(express.static(__dirname + '/public'));

var views = __dirname + '/public/views/';

var router = express.Router();

router.get("/", function (req, res) {
    res.sendFile(views + "index.html");
});

router.get("/search", function (req, res, next) {
    console.log("searching...", req.query);

    request({
        url: req.query.url,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            res.send(body);
        } else {
            console.log(error);
        }
    });
});

app.use(router);

app.use("*", function (req, res) {
    res.sendFile(views + "404.html");
});

app.listen(3000, function () {
    console.log("Live at Port 3000");
});